package cat.itb.character.repository;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import androidx.lifecycle.LiveData;
import androidx.room.Delete;
import androidx.room.Update;

import java.util.List;
import java.util.ListIterator;

import cat.itb.character.DAO.PersonatgeDAO;
import cat.itb.character.db.ApplicationDataBase;
import cat.itb.character.model.Personatge;

public class PersonatgeRepository {
    private PersonatgeDAO dao;
    private LiveData<List<Personatge>> personatges;

    public PersonatgeRepository(Context context){
        ApplicationDataBase db = ApplicationDataBase.getINSTANCE(context);
        this.dao = db.getDAO();
        personatges = dao.getPersonatges();
    }
    public void savePersonatge(Personatge personatge) { new InsertAsyncTask(dao).execute(personatge);}
    private static class InsertAsyncTask extends AsyncTask<Personatge,Void, Void>{
        PersonatgeDAO pDao;
        public InsertAsyncTask(PersonatgeDAO dao){
            pDao = dao;
        }

        @Override
        protected Void doInBackground(Personatge... personatges) {
            pDao.insert(personatges[0]);
            return null;
        }
    }

    public LiveData<List<Personatge>> getPersonatgesByOrderName() {
        return dao.getPersonatgesOrderByName();
    }

    public void updatePersonatge(Personatge personatge) { new UpdateAsyncTask(dao).execute(personatge);}
    private static class UpdateAsyncTask extends AsyncTask<Personatge,Void, Void>{
        PersonatgeDAO dao;
        public UpdateAsyncTask (PersonatgeDAO dao){this.dao = dao;}


        @Override
        protected Void doInBackground(Personatge... personatges) {
            dao.update(personatges[0]);
            return null;
        }
    }

    public void deletePersonatge (Personatge personatge) {new DeleteAsyncTask (dao).execute(personatge);}
    private class DeleteAsyncTask extends AsyncTask<Personatge, Void, Void>{
        PersonatgeDAO dao;
        public DeleteAsyncTask(PersonatgeDAO dao){this.dao = dao;}

        @Override
        protected Void doInBackground(Personatge... personatges) {
            dao.delete(personatges[0]);
            return null;
        }
    }
    public LiveData<List<Personatge>> getPersonatge(){return personatges;}
    public LiveData<Personatge> getPersonatgeById (int idPersonatge){
        return dao.getPersonatgeById(idPersonatge);

    }

}
