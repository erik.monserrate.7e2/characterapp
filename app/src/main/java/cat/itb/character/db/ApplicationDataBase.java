package cat.itb.character.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import cat.itb.character.DAO.PersonatgeDAO;
import cat.itb.character.model.Personatge;

@Database(entities = {Personatge.class}, version = 2)
public abstract class ApplicationDataBase extends RoomDatabase {
    private static volatile ApplicationDataBase INSTANCE;
    private static final Migration MIGRATION_1_2 = new Migration(1,2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE 'personatges' ADD COLUMN 'adapted' INTEGER NOT NULL DEFAULT 0");

        }
    };


    public static ApplicationDataBase getINSTANCE(Context context) {
        if (INSTANCE == null){
            synchronized (ApplicationDataBase.class){
                if (INSTANCE==null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),ApplicationDataBase.class,"personatge_database").addMigrations(MIGRATION_1_2).build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract PersonatgeDAO getDAO();
}
