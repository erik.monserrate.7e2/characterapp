package cat.itb.character.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import cat.itb.character.model.Personatge;

@Dao
public interface PersonatgeDAO {

    @Insert
    public void insert (Personatge personatge);

    @Query("SELECT * FROM personatges")
    public LiveData<List<Personatge>> getPersonatges();

    @Query("SELECT * FROM personatges WHERE id = :id")
    public LiveData<Personatge> getPersonatgeById(int id);

    @Query("SELECT*FROM personatges ORDER BY name")
    LiveData<List<Personatge>> getPersonatgesOrderByName();

    @Update
    void update (Personatge personatge);

    @Delete
    void delete (Personatge personatge);
}
