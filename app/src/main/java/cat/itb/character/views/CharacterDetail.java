package cat.itb.character.views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.character.R;
import cat.itb.character.model.Personatge;

public class CharacterDetail extends Fragment {

    PersonatgeAdapter adapter;
    private AddPersonatgeViewModel cViewModel;

    @BindView(R.id.detailimageProfile)
    ImageView detailimageProfile;
    @BindView(R.id.detailCharacterName)
    TextView detailCharacterName;
    @BindView(R.id.detailcharacterCategory)
    TextView detailcharacterCategory;
    @BindView(R.id.detailDescriptionCharacter)
    TextView detailDescriptionCharacter;
    @BindView(R.id.guideline)
    Guideline guideline;
    @BindView(R.id.statsView)
    TextView statsView;
    @BindView(R.id.imageView3)
    ImageView imageView3;
    @BindView(R.id.imageView4)
    ImageView imageView4;
    @BindView(R.id.imageView5)
    ImageView imageView5;
    @BindView(R.id.imageView6)
    ImageView imageView6;
    @BindView(R.id.detailStatHealth)
    TextView detailStatHealth;
    @BindView(R.id.detailStatAttack)
    TextView detailStatAttack;
    @BindView(R.id.detailStatCooldown)
    TextView detailStatCooldown;
    @BindView(R.id.detailStatSpeed)
    TextView detailStatSpeed;
    @BindView(R.id.guideline2)
    Guideline guideline2;
    @BindView(R.id.detailStarPowers)
    TextView detailStarPowers;
    @BindView(R.id.imageView7)
    ImageView imageView7;
    @BindView(R.id.imageView8)
    ImageView imageView8;
    @BindView(R.id.detailAbility1)
    TextView detailAbility1;
    @BindView(R.id.detailAbility2)
    TextView detailAbility2;
    @BindView(R.id.detailSuperAttack)
    TextView detailSuperAttack;
    @BindView(R.id.detailAttackName)
    TextView detailAttackName;
    @BindView(R.id.detailAttackDesc)
    TextView detailAttackDesc;
    @BindView(R.id.guideline3)
    Guideline guideline3;
    @BindView(R.id.guideline4)
    Guideline guideline4;
    @BindView(R.id.guideline5)
    Guideline guideline5;
    private int idPersonatge;

    private CharacterDetailViewModel mViewModel;

    public static CharacterDetail newInstance() {
        return new CharacterDetail();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.character_detail_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CharacterDetailViewModel.class);
        cViewModel = ViewModelProviders.of(this).get(AddPersonatgeViewModel.class);
        LiveData<Personatge> personatge = mViewModel.getPersonatgeById(idPersonatge);
        personatge.observe(this, this::onPersonatgeChanged);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        idPersonatge = CharacterDetailArgs.fromBundle(getArguments()).getIdPersonatge();
    }

    private void onPersonatgeChanged(Personatge personatge) {
        setTextViewsFromPersonatge(personatge);
    }

    private void setTextViewsFromPersonatge(Personatge personatge) {
        detailCharacterName.setText(personatge.getName());
        detailcharacterCategory.setText(personatge.getType());
        detailDescriptionCharacter.setText(personatge.getDescripcio());
        detailStatHealth.setText((personatge.getHealth() + ""));
        detailStatSpeed.setText(personatge.getSpeed() + "");
        detailStatAttack.setText(personatge.getAttack() + "");
        detailStatCooldown.setText(personatge.getReload() + "");
        detailAbility1.setText(personatge.getStarHability1());
        detailAbility2.setText(personatge.getStarHability2());
        detailAttackName.setText(personatge.getSuperAttack());
        detailAttackDesc.setText(personatge.getDescripcioAttack());
//        detailimageProfile.setImageURI(personatge.getUriProfilePicture());

    }

    @OnClick(R.id.btn_Edit)
    public void onClick () {
        NavDirections directions = CharacterDetailDirections.actionCharacterDetailToAddPersonatge(idPersonatge);
        Navigation.findNavController(this.getView()).navigate(directions);
    }

    @OnClick(R.id.btn_Delete)
    public void onViewClicked(View view) {
        mViewModel.deletePersonatge(idPersonatge);
        Navigation.findNavController(view).popBackStack();

    }
}
