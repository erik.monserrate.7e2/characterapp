package cat.itb.character.views;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import cat.itb.character.model.Personatge;
import cat.itb.character.repository.PersonatgeRepository;

public class CharacterDetailViewModel extends AndroidViewModel {
    private PersonatgeRepository repository;

    public CharacterDetailViewModel(@NonNull Application application) {
        super( application );
        repository = new PersonatgeRepository( application );
    }

    public LiveData<Personatge> getPersonatgeById(int idPersonatge){
        return repository.getPersonatgeById( idPersonatge );
    }

    public void deletePersonatge(int idPersonatge){
        repository.deletePersonatge(new Personatge(idPersonatge));
    }
    public LiveData<List<Personatge>> getPersonatgesOrderByName(){
        return repository.getPersonatgesByOrderName();
    }
}
