package cat.itb.character.views;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.character.R;
import cat.itb.character.model.Personatge;

public class PersonatgeAdapter extends RecyclerView.Adapter<PersonatgeAdapter.PersonatgeViewHolder> {
    private List<Personatge> personatges = new ArrayList<>();
    OnPersonatgeClickListener listener;

    public PersonatgeAdapter(){
    }


    public void setPersonatges(List<Personatge> personatges) {
        this.personatges = personatges;
        notifyDataSetChanged();
    }

    public void setListener(OnPersonatgeClickListener listener) {this.listener = listener;}

    @NonNull
    @Override
    public PersonatgeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.personatge_item,parent,false);
        return new PersonatgeViewHolder(view);
    }

    @Override
    public void onBindViewHolder (@NonNull PersonatgeViewHolder holder, int position){
        Personatge personatge = personatges.get(position);
        holder.item_titleName.setText(personatge.getName());
        holder.item_typeCharacter.setText(personatge.getType());
        holder.item_imageCharacter.setImageResource(personatge.getImageID());


    }
    @Override
    public int getItemCount(){
        int size = 0;
        if (personatges!=null) size= personatges.size();
        return size;
    }

    public class PersonatgeViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.item_titleName)
        TextView item_titleName;
        @BindView(R.id.item_TypeCharacter)
        TextView item_typeCharacter;
        @BindView(R.id.item_imageCharacter)
        ImageView item_imageCharacter;



        public PersonatgeViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick (R.id.personatge_Row)
        public void onPersonatgeViewClicked(){
            Personatge personatge = personatges.get(getAdapterPosition());
            if (listener!=null)
            listener.onPersonatgeViewClicked(personatge);
        }
    }
    public interface OnPersonatgeClickListener {
        void onPersonatgeViewClicked (Personatge personatge);
    }
}
