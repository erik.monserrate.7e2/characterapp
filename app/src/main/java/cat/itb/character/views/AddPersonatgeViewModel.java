package cat.itb.character.views;

import android.app.Application;
import android.widget.Adapter;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import cat.itb.character.model.Personatge;
import cat.itb.character.repository.PersonatgeRepository;

public class AddPersonatgeViewModel extends AndroidViewModel {
    private PersonatgeRepository personatgeRepository;
    private LiveData<List<Personatge>> personatges;
    private boolean isUpdate = false;

    public AddPersonatgeViewModel (@NonNull Application application){
        super(application);
        personatgeRepository = new PersonatgeRepository(application);
        personatges = personatgeRepository.getPersonatge();
    }
    public void savePersonatge(Personatge personatge){
        if (isUpdate){
            personatgeRepository.updatePersonatge(personatge);
        }
        else{
            personatgeRepository.savePersonatge(personatge);
        }
    }

    public LiveData<List<Personatge>> getPersonatgesOrderByName(){
        return personatgeRepository.getPersonatgesByOrderName();
    }

    public LiveData <List<Personatge>> getPersonatges(){return personatges;}

    public boolean isUpdate(int idPersonatge){
        isUpdate = idPersonatge !=-1;
        return isUpdate;
    }

    public LiveData<Personatge> getPersonatgeById(int idPersonatge){
        return personatgeRepository.getPersonatgeById(idPersonatge);
    }
}
