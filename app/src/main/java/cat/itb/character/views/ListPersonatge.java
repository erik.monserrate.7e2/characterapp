package cat.itb.character.views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.character.R;
import cat.itb.character.model.Personatge;

public class ListPersonatge extends Fragment {

    @BindView(R.id.recyclerViewList)
    RecyclerView recyclerViewList;

    PersonatgeAdapter adapter;

    private AddPersonatgeViewModel mViewModel;
    private CharacterDetailViewModel cViewModel;

    public static ListPersonatge newInstance() {
        return new ListPersonatge();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_personatge, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AddPersonatgeViewModel.class);
        cViewModel = ViewModelProviders.of(this).get(CharacterDetailViewModel.class);
        recyclerViewList.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerViewList.setLayoutManager(layoutManager);

        adapter = new PersonatgeAdapter();
        recyclerViewList.setAdapter(adapter);
        adapter.setListener(this::viewPersonatge);
        LiveData<List<Personatge>> personatges = mViewModel.getPersonatges();
        personatges.observe(this,this::onPersonatgesChanged);

        FloatingActionButton btnFiltre = getView().findViewById(R.id.filtre_Btn);
        btnFiltre.setOnClickListener(this::filtrar);

    }

    private void filtrar(View view){
        LiveData<List<Personatge>> liveData = mViewModel.getPersonatgesOrderByName();
        liveData.observe(this,this::onPersonatgesChanged);

    }

    private void viewPersonatge(Personatge personatge) {
        Integer idPersonatge = Integer.valueOf(personatge.getId());
        NavDirections direction = ListPersonatgeDirections.actionListpersonatgeToCharacterDetail(idPersonatge);
        Navigation.findNavController(this.getView()).navigate(direction);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    private void onPersonatgesChanged(List<Personatge> personatges) {
        adapter.setPersonatges(personatges);
        if (personatges.isEmpty()){
            Toast.makeText(getContext(), "No personatges", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.insert_Btn)
    public void onViewClicked() {
        Navigation.findNavController(recyclerViewList).navigate(R.id.action_listpersonatge_to_addPersonatge);
    }

}
