package cat.itb.character.views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Guideline;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.character.R;
import cat.itb.character.model.Personatge;

import static android.app.Activity.RESULT_OK;

public class addPersonatge extends Fragment {


    @BindView(R.id.imageAdd)
    ImageView imageView2;
    @BindView(R.id.nom_Brawl)
    EditText nomBrawl;
    @BindView(R.id.type_character)
    EditText typecharacter;
    @BindView(R.id.descripcio_Brawl)
    EditText descripcioBrawl;
    @BindView(R.id.guideline)
    Guideline guideline;
    @BindView(R.id.title_Stats)
    TextView titleStats;
    @BindView(R.id.image_Health)
    ImageView imageHealth;
    @BindView(R.id.image_Damage)
    ImageView imageDamage;
    @BindView(R.id.image_Reload)
    ImageView imageReload;
    @BindView(R.id.image_Speed)
    ImageView imageSpeed;
    @BindView(R.id.stats_Health)
    EditText statsHealth;
    @BindView(R.id.stats_Damage)
    EditText statsDamage;
    @BindView(R.id.stats_Reload)
    EditText statsReload;
    @BindView(R.id.stats_Speed)
    EditText statsSpeed;
    @BindView(R.id.guideline2)
    Guideline guideline2;
    @BindView(R.id.title_HabilitatEstel)
    TextView titleHabilitatEstel;
    @BindView(R.id.image_Star1)
    ImageView imageStar1;
    @BindView(R.id.image_Star2)
    ImageView imageStar2;
    @BindView(R.id.name_Hability1)
    EditText nameHability1;
    @BindView(R.id.name_Hability2)
    EditText nameHability2;
    @BindView(R.id.title_Attack)
    TextView titleAttack;
    @BindView(R.id.name_Attack)
    EditText nameAttack;
    @BindView(R.id.descripcio_Attack)
    EditText descripcioAttack;
    @BindView(R.id.btn_Save)
    MaterialButton btnSave;
    @BindView(R.id.guideline3)
    Guideline guideline3;
    @BindView(R.id.guideline4)
    Guideline guideline4;
    @BindView(R.id.guideline5)
    Guideline guideline5;
    @BindView(R.id.btn_addPhoto)
    FloatingActionButton btnAddPhoto;


    private AddPersonatgeViewModel mViewModel;
    int idPersonatge;

    public static addPersonatge newInstance() {
        return new addPersonatge();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_personatge, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        idPersonatge = addPersonatgeArgs.fromBundle(getArguments()).getIdPersonatge();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AddPersonatgeViewModel.class);
        if (mViewModel.isUpdate(idPersonatge)) {
          LiveData<Personatge> personatge = mViewModel.getPersonatgeById(idPersonatge);
           personatge.observe(this, this::fillEditTextWithData);}
    }
    // TODO: Use the ViewModel




    private void fillEditTextWithData(Personatge personatge) {
        nomBrawl.setText(personatge.getName());
        descripcioBrawl.setText(personatge.getDescripcio());
        typecharacter.setText( personatge.getType() );
        statsHealth.setText(personatge.getHealth() + "");
        statsDamage.setText(personatge.getAttack() + "");
        statsReload.setText(personatge.getReload() + "");
        statsSpeed.setText((personatge.getSpeed() + ""));
        nameHability1.setText(personatge.getStarHability1());
        nameHability2.setText(personatge.getStarHability2());
        nameAttack.setText(personatge.getSuperAttack());
        descripcioAttack.setText(personatge.getDescripcioAttack());
    }

    @OnClick(R.id.btn_Save)
    public void onClick() {
        Personatge personatge = getPersonatgeAtributes();
        if (idPersonatge != -1) {
            personatge.setId(idPersonatge);
        }
        mViewModel.savePersonatge(personatge);
        Navigation.findNavController(getView()).navigate(R.id.action_addPersonatge_to_listpersonatge);

    }

    private Personatge getPersonatgeAtributes() {
        String nameBrawl = nomBrawl.getEditableText().toString();
        String typeBrawl = typecharacter.getEditableText().toString();
        String descripcioAtaque = descripcioAttack.getEditableText().toString();
        String descripcio = descripcioBrawl.getEditableText().toString();

        String statHealth = statsHealth.getEditableText().toString();
        float health = 0;
        if (!statHealth.isEmpty()) health = Float.valueOf(statHealth);

        String statSpeed = statsSpeed.getEditableText().toString();
        float speed = 0;
        if (!statSpeed.isEmpty()) speed = Float.valueOf(statSpeed);

        String statDamage = statsDamage.getEditableText().toString();
        float damage = 0;
        if (!statDamage.isEmpty()) damage = Float.valueOf(statDamage);

        String statReload = statsReload.getEditableText().toString();
        float realoadFloat = 0;
        if(!statReload.isEmpty()) realoadFloat = Float.valueOf(statReload);


        String superAttack = nameAttack.getEditableText().toString();
        String hability1 = nameHability1.getEditableText().toString();
        String hability2 = nameHability2.getEditableText().toString();


        // String name,String type,String descripcioAttack, String descripcio, float health, float speed, float attack, float reload, String superAttack, String starHability1, String starHability2
        Personatge personatge = new Personatge(nameBrawl, typeBrawl, descripcioAtaque, descripcio, health, speed, damage, realoadFloat, superAttack, hability1, hability2);
        return personatge;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri path = data.getData();
            btnAddPhoto.setImageURI(path);
        }
    }

    @OnClick(R.id.btn_addPhoto)
    public void onViewClicked() {
        cargarImagen();
    }

    private void cargarImagen() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent.createChooser(intent, "Seleccione la Aplicación"), 10);

    }
}

