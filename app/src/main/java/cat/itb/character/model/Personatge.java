package cat.itb.character.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity (tableName = "Personatges")
public class Personatge {
    @PrimaryKey(autoGenerate = true)
    int id;
    String name;
    String type;
    int imageID;
    String descripcioAttack;
    String descripcio;
    float health,speed,attack,reload;
    String uriProfilePicture;
    String mainAttack,superAttack;
    String starHability1,StarHability2;

    public Personatge(String name,String type,String descripcioAttack, String descripcio, float health, float speed, float attack, float reload, String superAttack, String starHability1, String starHability2) {
        this.name = name;
        this.type = type;
        this.descripcioAttack = descripcioAttack;
        this.descripcio = descripcio;
        this.health = health;
        this.speed = speed;
        this.attack = attack;
        this.reload = reload;
        this.superAttack = superAttack;
        this.starHability1 = starHability1;
        StarHability2 = starHability2;
    }

    public Personatge() {
    }

    public Personatge(int idPersonatge) {
        this.id = idPersonatge;
    }

    public String getDescripcioAttack() {
        return descripcioAttack;
    }

    public void setDescripcioAttack(String descripcioAttack) {
        this.descripcioAttack = descripcioAttack;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getHealth() {
        return health;
    }

    public void setHealth(float health) {
        this.health = health;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getAttack() {
        return attack;
    }

    public void setAttack(float attack) {
        this.attack = attack;
    }

    public float getReload() {
        return reload;
    }

    public void setReload(float reload) {
        this.reload = reload;
    }

    public String getUriProfilePicture() {
        return uriProfilePicture;
    }

    public void setUriProfilePicture(String uriProfilePicture) {
        this.uriProfilePicture = uriProfilePicture;
    }

    public String getMainAttack() {
        return mainAttack;
    }

    public void setMainAttack(String mainAttack) {
        this.mainAttack = mainAttack;
    }

    public String getSuperAttack() {
        return superAttack;
    }

    public void setSuperAttack(String superAttack) {
        this.superAttack = superAttack;
    }

    public String getStarHability1() {
        return starHability1;
    }

    public void setStarHability1(String starHability1) {
        this.starHability1 = starHability1;
    }

    public String getStarHability2() {
        return StarHability2;
    }

    public void setStarHability2(String starHability2) {
        StarHability2 = starHability2;
    }
}
